package com.example.restinventory.springbootExercise.data.business.service;

import com.example.restinventory.springbootExercise.data.entity.Type;
import com.example.restinventory.springbootExercise.data.repository.ItemRepository;
import com.example.restinventory.springbootExercise.data.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TypeService {
    private TypeRepository typeRepository;
    private ItemRepository itemRepository;

    @Autowired
    public TypeService(TypeRepository typeRepository, ItemRepository itemRepository) {
        this.typeRepository = typeRepository;
        this.itemRepository = itemRepository;
    }

    public ResponseEntity getType(){
        Iterable<Type> types = typeRepository.findAll();
//        Map typeMap = new HashMap();
//        types.forEach(type->{
//            typeMap.put(type.getType_id(),type);
//        });
        List<Type> typeList = new ArrayList<>();
        types.forEach(type->{
            typeList.add(type);
        });

        return ResponseEntity.ok(typeList);
    }

    public ResponseEntity getType(long id){
        Optional<Type> type = typeRepository.findById(id);
        if(type.isPresent()){
            return ResponseEntity.ok(type.get());
        } return new ResponseEntity<>("No ID: "+id+" in database.", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Object> createType(Type type){
        Type newType = new Type();
        newType.setName(type.getName());
        newType.setDescription(type.getDescription());
//        newType.setCost(type.getCost());
        newType.setActive(type.getActive());
        newType.setDate_created(new java.sql.Date(System.currentTimeMillis()));
        newType.setCreated_by(type.getCreated_by());
        typeRepository.save(newType);

        return ResponseEntity.ok(newType);
    }

    public ResponseEntity<Object> updateType(Type type, long id){
        if(getType(id).getStatusCodeValue() == 200){
            Type newType = (Type) this.getType(id).getBody();
            newType.setName(type.getName());
//            newType.setCost(type.getCost());
            newType.setCreated_by(type.getCreated_by());
            newType.setDate_created(new java.sql.Date(System.currentTimeMillis()));
            newType.setDescription(type.getDescription());
            typeRepository.save(newType);
            return ResponseEntity.ok(getType(id).getBody());
        } else return new ResponseEntity<>(this.getType(id).getBody(), HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Object> deleteType(long id){
        if(getType(id).getStatusCodeValue() == 200){
            typeRepository.deleteById(id);
            return ResponseEntity.ok("Type id: "+id+" has been deleted.");
        } else return new ResponseEntity<>(("No type id: "+id+" found"),HttpStatus.NOT_FOUND);
    }

    public String activateType(long id){
        ResponseEntity re = this.getType(id);
        if(re.getStatusCodeValue() == 200){
            Type type = (Type) re.getBody();
            type.setActive(1);
            typeRepository.save(type);
            return "Successfully Activated ID: "+id;
        }
        return "ERROR activating ID: "+id+" statuscode: "+re.getStatusCodeValue();
    }

    public String deactivateType(long id){
        ResponseEntity re = this.getType(id);
        if(re.getStatusCodeValue() == 200){
            //        Type type = typeRepository.findById(id).get();
            Type type = (Type) re.getBody();
            type.setActive(0);
            typeRepository.save(type);
            return "Successfully Deactivated ID: "+id;
        }
        return "ERROR deactivating ID: "+id+" statuscode: "+re.getStatusCodeValue();
    }
}
