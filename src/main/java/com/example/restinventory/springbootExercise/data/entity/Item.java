package com.example.restinventory.springbootExercise.data.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="item_data")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ITEM_ID")
    private long item_id;
    @Column(name = "name")
    private String name;
    @Column(name = "TYPE_ID")
    private int type_id;
//    @Column(name = "description")
//    private String description;
    @Column(name = "cost", columnDefinition = "default 0")
    private int cost;
    @Column(name = "date_created")
    private Date date;
    @Column(name = "count", columnDefinition = "default 0")
    private int count;
    @Column(name = "active", columnDefinition = "default 1")
    private int active;


    public long getItem_id() {
        return item_id;
    }

    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
