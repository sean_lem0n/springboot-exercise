package com.example.restinventory.springbootExercise.data.business.domain;

import java.util.Date;

public class ItemDomain {
    private long itemId;
    private String name;
    private long typeId;
    private String description;
    private long cost;
    private Date date_created;
    private int count;
    private int item_active;
    private int type_active;

    public int getType_active() {
        return type_active;
    }

    public void setType_active(int type_active) {
        this.type_active = type_active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getItem_active() {
        return item_active;
    }

    public void setItem_active(int item_active) {
        this.item_active = item_active;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }
}
