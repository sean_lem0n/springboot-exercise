package com.example.restinventory.springbootExercise.data.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "type_data")
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TYPE_ID")
    private long type_id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
//    @Column(name = "cost")
//    private long cost;
    @Column(name = "created_by")
    private String created_by;
    @Column(name = "date_created")
    private Date date_created;
    @Column(name = "active")
    private int active;


    public long getType_id() {
        return type_id;
    }

    public void setType_id(long type_id) {
        this.type_id = type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public long getCost() {
//        return cost;
//    }
//
//    public void setCost(long cost) {
//        this.cost = cost;
//    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
}
