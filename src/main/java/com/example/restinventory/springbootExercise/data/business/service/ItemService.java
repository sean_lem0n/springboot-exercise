package com.example.restinventory.springbootExercise.data.business.service;

import com.example.restinventory.springbootExercise.data.business.domain.ItemDomain;
import com.example.restinventory.springbootExercise.data.entity.Item;
import com.example.restinventory.springbootExercise.data.entity.Type;
import com.example.restinventory.springbootExercise.data.repository.ItemRepository;
import com.example.restinventory.springbootExercise.data.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ItemService {
    private ItemRepository itemRepository;
    private TypeRepository typeRepository;
    private TypeService typeService;

    @Autowired
    public ItemService(ItemRepository itemRepository, TypeRepository typeRepository, TypeService typeService) {
        this.typeRepository = typeRepository;
        this.itemRepository = itemRepository;
        this.typeService = typeService;
    }

    public Map<Long, ItemDomain> getAllItemMap(){
        Iterable<Type> types = this.typeRepository.findAll();
        Map<Long, Type> typeMap = new HashMap<>();
        types.forEach(type-> typeMap.put(type.getType_id(),type));

        Iterable<Item> items = this.itemRepository.findAll();
        Map<Long, ItemDomain> itemDomainMap = new HashMap<>();
        items.forEach( item->{
            ItemDomain itemDomain = new ItemDomain();
            itemDomain.setItemId(item.getItem_id());
            itemDomain.setName(item.getName());
            itemDomain.setTypeId(item.getType_id());
            itemDomain.setCost(item.getCost());
            itemDomain.setDate_created(item.getDate());
            itemDomain.setCount(item.getCount());
            itemDomain.setItem_active(item.getActive());

            itemDomain.setDescription(typeMap.get(itemDomain.getTypeId()).getDescription());
            itemDomain.setType_active(typeMap.get(itemDomain.getTypeId()).getActive());

            itemDomainMap.put(item.getItem_id(), itemDomain);
        });
        return itemDomainMap;
    }

    public List<ItemDomain> getItem(){
        Map<Long, ItemDomain> itemDomainMap = this.getAllItemMap();

        List<ItemDomain> itemList = new ArrayList<>();
        for(Long itemId:itemDomainMap.keySet()){
            itemList.add(itemDomainMap.get(itemId));
        }
        return itemList;
    }

    public ResponseEntity<Object> getItemById(long itemId){
        Map<Long, ItemDomain> itemDomainMap = this.getAllItemMap();
        if(itemDomainMap.containsKey(itemId)) {
            return ResponseEntity.ok(itemDomainMap.get(itemId));
        }
        else
            return new ResponseEntity<>("No ID: "+itemId+" in database", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Object> deleteById(long itemId){
        Map<Long, ItemDomain> itemDomainMap = this.getAllItemMap();
        if(itemDomainMap.containsKey(itemId)) {
            itemRepository.deleteById(itemId);
            return ResponseEntity.ok("Successfully Deleted: "+itemId);
        } else {
            return new ResponseEntity<>("INVALID ID "+itemId+". No user found.",HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Object> updateItem(Item item, long itemId){
        if(this.getItemById(itemId).getStatusCodeValue() == 200) {
            Item newItem = new Item();
            newItem.setItem_id(itemId);
            newItem.setName(item.getName());
            newItem.setType_id(item.getType_id());
            newItem.setCount(item.getCount());
            newItem.setCost(item.getCost());
            newItem.setDate(new java.sql.Date(System.currentTimeMillis()));
            newItem.setActive(item.getActive());
//            newItem.setDescription(item.getDescription());
            if (this.typeService.getType(item.getType_id()).getStatusCodeValue() == 200) {
                itemRepository.save(newItem);
//                Type newType = (Type) typeService.getType(item.getType_id()).getBody();
//                newType.setDescription(item.getDescription());
//                typeRepository.save(newType);
                return ResponseEntity.ok(this.getItemById(itemId));
            } else {
                return new ResponseEntity<>("TypeID not found. Cannot Update!", HttpStatus.NOT_FOUND);
            }
        } return new ResponseEntity<>("ItemID not found. Cannot Update", HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Object> createItem(Item item){
            Item newItem = new Item();
//            newItem.setItem_id(itemId); GETUUID
            newItem.setName(item.getName());
            newItem.setType_id(item.getType_id());
            newItem.setCount(item.getCount());
            newItem.setCost(item.getCost());
            newItem.setDate(new java.sql.Date(System.currentTimeMillis()));
            newItem.setActive(item.getActive());
//            newItem.setDescription(item.getDescription());
            if (this.typeService.getType(item.getType_id()).getStatusCodeValue() == 200) {
                itemRepository.save(newItem);
                return ResponseEntity.ok(newItem);
            } else {
                return new ResponseEntity<>("TypeID not found. Cannot Create!", HttpStatus.NOT_FOUND);
            }
    }

    public Map<String,Integer> totalCost(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Total Cost", this.itemRepository.totalCost());
        return map;
    }

    public HashMap<String,Integer> totalCount(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Total Count", this.itemRepository.totalCount());
        return map;
    }

    public String activateItem(long id) {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            Item updatedItem = item.get();
            updatedItem.setActive(1);
            itemRepository.save(updatedItem);
            return "Successfully Activated item ID: " + id;
        } else {
            return "ERROR activating item ID: " + id;
        }
    }

    public String deactivateItem(long id)  {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            Item updatedItem = item.get();
            updatedItem.setActive(0);
            itemRepository.save(updatedItem);
            return "Successfully Deactivated item ID: " + id;
        } else {
            return "ERROR activating item ID: " + id;
        }
    }
}
