package com.example.restinventory.springbootExercise.data.business.controller;

import com.example.restinventory.springbootExercise.data.business.domain.ItemDomain;
import com.example.restinventory.springbootExercise.data.business.service.ItemService;
import com.example.restinventory.springbootExercise.data.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ItemController {
    private final String template = "TEST";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private ItemService itemService;

    @RequestMapping(method = RequestMethod.GET, value = "/item")
    public List<ItemDomain> getItem(){
        return this.itemService.getItem();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/item/{itemId}")
    public ResponseEntity<Object> getItemById(@PathVariable long itemId){
        return this.itemService.getItemById(itemId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/item")
    public ResponseEntity<Object> createItem(@RequestBody Item item){
        return this.itemService.createItem(item);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/item/{itemId}")
    public ResponseEntity<Object> deleteItem(@PathVariable long itemId){
        return this.itemService.deleteById(itemId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/item//activate/{id}")
    public String activateItem(@PathVariable long id){
        return this.itemService.activateItem(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/item/deactivate/{id}")
    public String deactivateItem(@PathVariable long id){
        return this.itemService.deactivateItem(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/item/{itemId}")
    public ResponseEntity<Object> updateItem(@RequestBody Item item, @PathVariable long itemId){
        return this.itemService.updateItem(item, itemId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getTotalCost")
    public Map<String,Integer> getTotalCost(){
            return this.itemService.totalCost();
        }

    @RequestMapping(method = RequestMethod.GET, value = "/getTotalCount")
    public Map<String, Integer> getTotalCount(){
        return this.itemService.totalCount();
    }

}
