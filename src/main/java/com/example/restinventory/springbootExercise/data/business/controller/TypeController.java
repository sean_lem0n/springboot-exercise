package com.example.restinventory.springbootExercise.data.business.controller;

import com.example.restinventory.springbootExercise.data.business.service.TypeService;
import com.example.restinventory.springbootExercise.data.entity.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class TypeController {
    @Autowired
    private TypeService typeService;


    @RequestMapping(method = RequestMethod.GET, value = "/type")
    public ResponseEntity<Object> getType(){
        return typeService.getType();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/type/{id}")
    public ResponseEntity getType(@PathVariable int id){
        return this.typeService.getType(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/type")
    public ResponseEntity<Object> createType(@RequestBody Type type){
        return typeService.createType(type);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/type/{id}")
    public ResponseEntity<Object> updateType(@RequestBody Type type, @PathVariable long id){
        return typeService.updateType(type, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/type/{id}")
    public ResponseEntity<Object> deleteType(@PathVariable long id){
        return typeService.deleteType(id);
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/type/activate/{id}")
    public String activateType(@PathVariable int id) {
        return this.typeService.activateType(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/type/deactivate/{id}")
    public String deactivateType(@PathVariable int id) { return this.typeService.deactivateType(id); }
}
