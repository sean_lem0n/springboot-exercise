package com.example.restinventory.springbootExercise.data.repository;

import com.example.restinventory.springbootExercise.data.business.domain.ItemDomain;
import com.example.restinventory.springbootExercise.data.entity.Item;
import com.sun.xml.bind.v2.model.core.ID;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    @Query(value = "SELECT i.item_id, i.name, t.type_id, t.description, i.cost,  t.date_created, i.count, i.active FROM item_data i " +
            "LEFT JOIN type_data t on i.typeId=t.typeId", nativeQuery = true)
    List<Item> getAllItemList();

    @Query(value = "SELECT SUM(cost) FROM" +
            "( SELECT i.item_id, i.name, t.type_id, t.description, i.cost,  t.date_created, i.count, t.active " +
            "FROM item_data i LEFT JOIN type_data t on i.type_id=t.type_id ) q", nativeQuery = true)
    Integer totalCost();

    @Query(value = "select count(1) FROM " +
            "( SELECT i.item_id, i.name, t.type_id, t.description, i.cost,  t.date_created, i.count, t.active " +
            "FROM item_data i LEFT JOIN type_data t on i.type_id=t.type_id ) q", nativeQuery = true)
    Integer totalCount();

}
