package com.example.restinventory.springbootExercise.data.repository;

import com.example.restinventory.springbootExercise.data.entity.Type;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends CrudRepository<Type, Long> {

}
